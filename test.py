import unittest
from main import summarizer


class Test(unittest.TestCase):

    def test_summarizer(self):
        assert summarizer(2, 3, 4) == 9


if __name__ == "__main__":
    unittest.main()
